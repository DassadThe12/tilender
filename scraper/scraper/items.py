# Define here the models for your scraped items
#
# See documentation in:
# https://docs.scrapy.org/en/latest/topics/items.html

import scrapy


class CersanitTile(scrapy.Item):
    shop = scrapy.Field()
    shop_url = scrapy.Field()
    name = scrapy.Field()
    image_url = scrapy.Field()
    average_color = scrapy.Field()
    pattern = scrapy.Field()
    average_color_name = scrapy.Field()
    tags = scrapy.Field()
    normalized_tags = scrapy.Field()

    def __repr__(self) -> str:
        return self["name"]


class CastoramaTile(scrapy.Item):
    shop = scrapy.Field()
    shop_url = scrapy.Field()
    name = scrapy.Field()
    image_url = scrapy.Field()
    average_color = scrapy.Field()
    average_color_name = scrapy.Field()
    type = scrapy.Field()
    tags = scrapy.Field()
    normalized_tags = scrapy.Field()

    def __repr__(self) -> str:
        return self["name"]