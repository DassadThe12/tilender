import json

import requests
import scrapy
from scrapy.http.request import Request

from scraper.items import CastoramaTile


class Castorama(scrapy.Spider):
    name = "Castorama"
    allowed_domains = ["www.castorama.pl"]
    dl_service = requests

    def start_requests(self):
        page_size = 47
        floor = (
            "https://www.castorama.pl/api/rest/headless/public/categories/products?"
            + "searchCriteria[currentPage]={}&"
            + "searchCriteria[filterGroups][0][filters][0][conditionType]=eq&"
            + "searchCriteria[filterGroups][0][filters][0][field]=category&"
            + "searchCriteria[filterGroups][0][filters][0][value]=2037&"
            + "searchCriteria[pageSize]=47&"
            + "searchCriteria[sortOrders][0][direction]=desc&"
            + "searchCriteria[sortOrders][0][field]=promoted&storeId=default"
        )
        wall = (
            "https://www.castorama.pl/api/rest/headless/public/categories/products?"
            + "searchCriteria[currentPage]={}&"
            + "searchCriteria[filterGroups][0][filters][0][conditionType]=eq&"
            + "searchCriteria[filterGroups][0][filters][0][field]=category&"
            + "searchCriteria[filterGroups][0][filters][0][value]=2038&"
            + "searchCriteria[pageSize]=47&"
            + "searchCriteria[sortOrders][0][direction]=desc&"
            + "searchCriteria[sortOrders][0][field]=promoted&storeId=default"
        )
        universal = (
            "https://www.castorama.pl/api/rest/headless/public/categories/products?"
            + "searchCriteria[currentPage]={}&"
            + "searchCriteria[filterGroups][0][filters][0][conditionType]=eq&"
            + "searchCriteria[filterGroups][0][filters][0][field]=category&"
            + "searchCriteria[filterGroups][0][filters][0][value]=8259&"
            + "searchCriteria[pageSize]=47&"
            + "searchCriteria[sortOrders][0][direction]=desc&"
            + "searchCriteria[sortOrders][0][field]=promoted&storeId=default"
        )
        yield scrapy.Request(
            floor.format(1),
            callback=self.prepare_all_page_requests,
            cb_kwargs={"url_template": floor, "page_size": page_size, "type": "floor"},
        )
        yield scrapy.Request(
            wall.format(1),
            callback=self.prepare_all_page_requests,
            cb_kwargs={"url_template": wall, "page_size": page_size, "type": "wall"},
        )
        yield scrapy.Request(
            universal.format(1),
            callback=self.prepare_all_page_requests,
            cb_kwargs={
                "url_template": universal,
                "page_size": page_size,
                "type": "universal",
            },
        )

    def prepare_all_page_requests(self, response, url_template, page_size, type):
        data = json.loads(response.text)
        pages = int(data["all"] / page_size) + 1

        for i in range(1, pages + 1):
            yield scrapy.Request(
                url_template.format(i),
                callback=self.parse_pages_json,
                cb_kwargs={"type": type},
            )
        # data = json.loads(response.text)
        # for i in data["items"]:
        #     yield CastoramaTile(
        #         shop="Castorama",
        #         shop_url=i["url"],
        #         name=i["name"],
        #         image_url=i["image"],
        #         tags=i,
        #         type=type,
        #     )

    def parse_pages_json(self, response, type):
        data = json.loads(response.text)
        for item in data["items"]:
            yield Request(
                item["url"],
                callback=self.parse_single_page,
                cb_kwargs={"type": type, "json_item": item},
            )

    def parse_single_page(self, response, type, json_item):
        data = {}

        tables = response.xpath(
            '//*[@id="specificationTab"]/section/section[2]/div/div'
        )
        for t in tables:
            keys = t.xpath("./table/tbody/tr/td[1]/text()").getall()
            keys = [k.strip() for k in keys]

            elems = t.xpath("./table/tbody/tr/td[2]/text()").getall()
            elems = [e.strip() for e in elems]
            data = {**data, **{k: e for k, e in zip(keys, elems)}}

        yield CastoramaTile(
            shop="Castorama",
            shop_url=response.url,
            name=json_item["name"],
            image_url=json_item["image"],
            tags=data,
            type=type,
        )
