import requests
import scrapy

from scraper.items import CersanitTile


class Cersanit(scrapy.Spider):
    name = "Cersanit"
    allowed_domains = ["www.cersanit.com.pl"]
    start_urls = ["http://www.cersanit.com.pl/plytki-ceramiczne/?page=99999"]
    dl_service = requests

    def parse(self, response):
        root_elem = response.selector.xpath(
            '//*[@id="arrange-list"]/div/div/div[2]/div[3]/div/div[1]/ul'
        )
        collecton_urls = root_elem.xpath("./li/div/div/a/@href").getall()

        for url in collecton_urls:
            yield scrapy.Request(
                response.urljoin(url), callback=self.parse_collection_page
            )

    def parse_collection_page(self, response):
        root_elem = response.selector.xpath(
            "/html/body/div/section[2]/div[2]/div/div/div/div[3]/div[1]/div/div[1]/ul"
        )
        tile_urls = root_elem.xpath("./li/div/div/a/@href").getall()
        for url in tile_urls:
            yield scrapy.Request(
                response.urljoin(url), callback=self.parse_tile_details_page
            )

    def parse_tile_details_page(self, response):
        tile_name = response.selector.xpath(
            "/html/body/div/section[2]/div[2]/div/div/div/div[1]/div[1]/div[1]/h1/text()"
        ).get()

        image_url = response.selector.xpath(
            '//*[@id="content"]/div[2]/div/div/div/div[1]/div[1]/div[2]/div/div[1]/div/div/div/a/@href'
        ).get()
        image_url = response.urljoin(image_url)

        table_root_elem = response.selector.xpath(
            '//*[@id="content"]/div[2]/div/div/div/div[1]/div[1]/div[3]/div[3]'
        )
        table_elem = table_root_elem.xpath("./div/div/div/div[@class='table']")
        table_data = {}
        for t in table_elem:
            d = t.xpath(
                "./div/div[@class='td label ' or @class='td label is-textWrap js-toggle']/div[not(@class)]/text()"
            ).getall()
            if len(d) == 0:
                d = t.xpath(
                    "./div/div[@class='td label ' or @class='td label is-textWrap js-toggle']/text()[1]"
                ).getall()
            d = [d.strip() for d in d]

            c = t.xpath("./div/div[@class='td value']/text()").getall()
            c = [c.strip() for c in c]

            # Remove Empty rows, because cersanit's page contains random elements with no text in the table with details
            c = [c for c in c if c != ""]

            tags = {desc: value for desc, value in zip(d, c)}
            table_data = {**table_data, **tags}

        ret = CersanitTile(
            shop="Cersanit",
            name=tile_name,
            shop_url=response.url,
            image_url=image_url,
            pattern=table_data["Wzór/design"],
            tags=table_data,
        )
        return ret
