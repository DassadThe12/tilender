import hashlib
import json
import logging

import pydantic
from django.conf import settings
from django.forms.models import model_to_dict
from djongo import models
from google.auth.exceptions import DefaultCredentialsError
from google.cloud import firestore

logger = logging.getLogger(__name__)


class DjangoTile(models.Model):
    class Meta:
        app_label = "scraper"

    shop = models.CharField()
    shop_url = models.CharField()
    name = models.CharField()
    image_url = models.CharField()
    average_color = models.CharField(max_length=7)
    average_color_name = models.CharField()
    pattern = models.CharField()
    tags = models.JSONField()
    normalized_tags = models.JSONField()
    type = models.CharField()
    hash = models.CharField()

    def save(self, *args, **kwargs) -> None:
        if settings.USE_FIREBASE:
            pass
        else:
            self.hash = hashlib.sha256(
                json.dumps(model_to_dict(self), sort_keys=True).encode("utf8")
            ).hexdigest()
            super().save(*args, **kwargs)


class FirebaseTile(pydantic.BaseModel):
    shop: str
    shop_url: str
    name: str
    image_url: str
    average_color: str
    average_color_name: str
    pattern = ""
    tags: dict
    normalized_tags: dict
    type: str

    @property
    def collection(self):

        if settings.USE_FIREBASE:
            try:
                return firestore.Client().collection(settings.FIREBASE_COLLECTION)
            except DefaultCredentialsError as e:
                logging.info(f"Cannot login to appengine.\n{repr(e)}")

    @property
    def hash(self):
        return hashlib.sha256(
            json.dumps(self.dict(), sort_keys=True).encode("utf8")
        ).hexdigest()

    @property
    def exists(self) -> bool:
        return self.collection.document(self.hash).get().exists

    def save(self):
        if settings.USE_FIREBASE:
            self.collection.document(self.hash).set(self.dict())
        else:
            pass
