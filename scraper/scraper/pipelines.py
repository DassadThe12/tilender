# Define your item pipelines here
#
# Don't forget to add your pipeline to the ITEM_PIPELINES setting
# See: https://docs.scrapy.org/en/latest/topics/item-pipeline.html


import asyncio
import io

import requests
import scrapy
from django.conf import settings
from itemadapter import ItemAdapter
from PIL import Image
from scrapy.exceptions import DropItem

import scraper.colors as colors
from scraper.django_settings import USE_FIREBASE
from scraper.items import CastoramaTile
from scraper.items import CersanitTile
from scraper.models import DjangoTile
from scraper.models import FirebaseTile

items_yielded = 0
item_limit = 100


def drop_overflow_items(argument=None):
    def drop_overflow_items_decorator(function):
        def wrapper(*args, **kwargs):
            global items_yielded
            global item_limit

            if items_yielded < item_limit:
                items_yielded += 1
                result = function(*args, **kwargs)
            else:
                raise DropItem()

            return result

        return wrapper

    return drop_overflow_items_decorator


class AverageImageColor:
    # @drop_overflow_items()
    def process_item(self, item, spider):
        if item.get("image_url") is not None:
            headers = {
                "User-Agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.75 Safari/537.36"
            }
            r = spider.dl_service.get(item["image_url"], headers=headers)
            raw = io.BytesIO(r.content)
            im = Image.open(raw)
            im = im.convert("RGB")
            im = im.resize((1, 1))
            color = im.getpixel((0, 0))

            item["average_color"] = f"#{color[0]:0>2X}{color[1]:0>2X}{color[2]:0>2X}"
            item["average_color_name"] = colors.get_closest_color_name(
                item["average_color"]
            )
            raw.close()

        return item


class NormalizeTags:
    @classmethod
    def get_tag(self, item, f_names):
        i_tags = ItemAdapter(item)["tags"]
        field = [i_tags[name] for name in f_names if i_tags.get(name) is not None]
        if len(field) != 1:
            return ""
        return field[0]

    @classmethod
    def frost_resistance(self, item):
        f_names = ["Mrozoodporność", "Płytka mrozoodporna"]

        field = self.get_tag(item, f_names)
        item["normalized_tags"]["mrozoodpornosc"] = field.lower().strip() == "tak"
        return item

    @classmethod
    def surface_type(self, item):
        f_names = ["Rodzaj powierzchni"]

        field = self.get_tag(item, f_names)
        item["normalized_tags"]["rodzaj_powierzchni"] = field.lower().strip()
        return item

    @classmethod
    def application_area(self, item):
        f_names = ["Przeznaczenie", "Obszary zastosowania"]

        field = self.get_tag(item, f_names)
        item["normalized_tags"]["wewnatrz"] = "wewnątrz" in field.lower().strip()
        item["normalized_tags"]["zewnatrz"] = "zewnątrz" in field.lower().strip()
        return item

    @classmethod
    def ammount_in_package(self, item):
        f_names = [
            "Liczba sztuk w opakowaniu",
        ]

        field = self.get_tag(item, f_names)
        item["normalized_tags"]["liczba_sztuk"] = field.lower().strip()
        return item

    @classmethod
    def guarantee(self, item):
        f_names = [
            "Liczba sztuk w opakowaniu",
        ]

        field = self.get_tag(item, f_names)
        item["normalized_tags"]["gwarancja"] = field.lower().strip()
        return item

    @classmethod
    def thicknes(self, item):
        f_names = ["Grubość produktu", "Grubość płytki"]

        field = self.get_tag(item, f_names)
        item["normalized_tags"]["grubosc"] = field.lower().strip()
        return item

    @classmethod
    def dimensions(self, item):
        f_names = ["Wymiary płytki", "Format produktu"]

        field = self.get_tag(item, f_names)
        item["normalized_tags"]["wymiary"] = field.lower().strip()
        return item

    def process_item(self, item, spider):
        if type(item.get("tags", None)) == dict:
            item["normalized_tags"] = {}
            item = self.frost_resistance(item)
            item = self.surface_type(item)
            item = self.application_area(item)
            item = self.ammount_in_package(item)
            item = self.guarantee(item)
            item = self.thicknes(item)
            item = self.dimensions(item)

        return item


class StoreInMongoDB:
    def process_item(self, item, spider):
        if type(item) == CersanitTile:
            ia = ItemAdapter(item)
            count = DjangoTile.objects.filter(**ia.asdict()).count()
            if count <= 0:
                DjangoTile(**ia.asdict(), type="universal").save()

        if type(item) == CastoramaTile:
            ia = ItemAdapter(item)
            count = DjangoTile.objects.filter(**ia.asdict()).count()
            if count <= 0:
                DjangoTile(**ia.asdict()).save()

        return item


class StoreInFirebase:
    def process_item(self, item, spider):
        if type(item) == CersanitTile:
            ia = ItemAdapter(item)
            tile = FirebaseTile(**ia.asdict(), type="universal")
            if not tile.exists:
                tile.save()

        if type(item) == CastoramaTile:
            ia = ItemAdapter(item)
            tile = FirebaseTile(**ia.asdict())
            if not tile.exists:
                tile.save()

        return item
