import django
from django.conf import settings

import scraper.django_settings as defaults

if not settings.configured:
    settings.configure(defaults)
    django.setup()
