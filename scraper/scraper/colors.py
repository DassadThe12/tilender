import json
import math
import os
import pathlib

colors = json.load(
    open(os.path.join(pathlib.Path(__file__).parent.absolute(), "colors.json"))
)


def get_closest_color_name(color):
    r, g, b = tuple(int(color[i : i + 2], 16) for i in (1, 3, 5))
    min_dist = 99999999999
    ret = ""
    for k, v in colors.items():
        x, y, z = tuple(int(v[i : i + 2], 16) for i in (1, 3, 5))
        e1 = (r - x) ** 2
        e2 = (g - y) ** 2
        e3 = (b - z) ** 2
        dist = math.sqrt(e1 + e2 + e3)
        if dist < min_dist:
            ret = k
            min_dist = dist
    return ret
