import os
import pathlib
from unittest import mock

import asgiref.sync as aconv
import pytest
import scrapy

from scraper.items import CastoramaTile
from scraper.items import CersanitTile
from scraper.models import DjangoTile
from scraper.pipelines import AverageImageColor
from scraper.pipelines import StoreInMongoDB


@pytest.mark.django_db
class TestStoreInMongoDB:
    def test_does_nothing_if_item_is_not_a_whateverTile(self):
        class NotTileItem(scrapy.Item):
            name = scrapy.Field()

        item_before = NotTileItem(name="test")
        db_before = DjangoTile.objects.count()

        item_after = StoreInMongoDB().process_item(item_before, None)
        db_after = DjangoTile.objects.count()

        assert db_before == db_after
        assert item_before == item_after

    def test_insert_castoramaTile_into_db(self):
        item_before = CastoramaTile(
            {
                "shop": "Castorama",
                "shop_url": "https://www.castorama.pl/",
                "name": "Gres Lomero Ceramstic 120 x 60 cm biały mat 1,44 m2",
                "image_url": "",
                "average_color": "#f9f9f9",
                "average_color_name": "alabastrowy",
                "tags": {
                    "brand": "Ceramstic",
                },
                "type": "floor",
            }
        )
        db_before = DjangoTile.objects.count()

        item_after = StoreInMongoDB().process_item(item_before, None)
        db_after = DjangoTile.objects.count()

        assert db_before + 1 == db_after
        assert item_before == item_after

    def test_insert_cersanitTile_into_db(self):
        item_before = CersanitTile(
            {
                "shop": "Castorama",
                "shop_url": "https://www.castorama.pl/",
                "name": "Gres Lomero Ceramstic 120 x 60 cm biały mat 1,44 m2",
                "image_url": "",
                "average_color": "#f9f9f9",
                "average_color_name": "alabastrowy",
                "tags": {
                    "brand": "Ceramstic",
                },
            }
        )
        db_before = DjangoTile.objects.count()

        item_after = StoreInMongoDB().process_item(item_before, None)
        db_after = DjangoTile.objects.count()

        assert db_before + 1 == db_after
        assert item_before == item_after


@pytest.fixture
def spider_factory():
    def __factory(file):
        class testSpider(scrapy.Spider):
            name = "test"
            allowed_domains = ["nope"]
            start_urls = ["http://localhost"]
            dl_service = mock.Mock()

            def __init__(self) -> None:
                super().__init__()
                self.dl_service.configure_mock(**{"get": self.get_file})

            def get_file(self, url, headers):
                path = pathlib.Path(__file__).parent.absolute()

                class ret:
                    content = open(os.path.join(path, "files", file), "rb").read()

                return ret()

        return testSpider()

    return __factory


class TestAverageImageColor:
    def test_if_item_has_no_image_url_field_do_nothing(self):
        class testSpider(scrapy.Spider):
            name = "test"
            allowed_domains = ["nope"]
            start_urls = ["http://localhost"]

        class NotTileItem(scrapy.Item):
            name = scrapy.Field()

        spider = testSpider()
        item_before = NotTileItem(name="test")
        item_after = AverageImageColor().process_item(item_before, spider)
        assert item_before == item_after

    # 下にこのくそがは6時間かかりました。腹切りはします。はああああああああああああああああああああああああああああああああああああああああああああ
    @pytest.mark.parametrize(
        "file",
        [
            f
            for f in list(
                os.walk(os.path.join(pathlib.Path(__file__).parent.absolute(), "files"))
            )[0][2]
        ],
    )
    def test_item_has_image_url(self, file, spider_factory):
        class ImageUrlItem(scrapy.Item):
            image_url = scrapy.Field()
            average_color = scrapy.Field()
            average_color_name = scrapy.Field()

        spider = spider_factory(file)

        item_before = ImageUrlItem(image_url="http://image")
        item_after = AverageImageColor().process_item(item_before, spider)

        item_before["average_color"] = f"#{file}"
        item_before["average_color_name"] = item_after["average_color_name"] = "removed"
        assert item_before == item_after
