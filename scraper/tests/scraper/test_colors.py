import json
import math
import os
import pathlib

import hypothesis.strategies as st
import pytest
from hypothesis import given

import scraper.colors as c


def test_colors_populated_from_json():
    path = os.path.join(
        pathlib.Path(__file__).parent.absolute(), "..", "..", "scraper", "colors.json"
    )
    from_file = json.load(open(path))
    assert c.colors == from_file


@pytest.fixture(scope="module")
def simplify_colors():
    c.colors = {
        "czarny": "#000000",
        "czerwony": "#FF0000",
        "zielony": "#00FF00",
        "niebieski": "#0000FF",
        "biały": "#FFFFFF",
    }
    return None


class TestGetClosestColor:
    @given(
        r=st.integers(max_value=255, min_value=0),
        g=st.integers(max_value=255, min_value=0),
        b=st.integers(max_value=255, min_value=0),
    )
    def test_returns_closest_color_name_if_rgb_was_kartesian_space(
        self, r, b, g, simplify_colors
    ):
        hex_color = f"#{r:0>2X}{g:0>2X}{b:0>2X}"
        mini = 99999999999
        ret = ""
        for k, v in c.colors.items():
            x, y, z = tuple(int(v[i : i + 2], 16) for i in (1, 3, 5))
            distance = math.sqrt((r - x) ** 2 + (g - y) ** 2 + (b - z) ** 2)
            if distance < mini:
                ret = k
                mini = distance

        result = c.get_closest_color_name(hex_color)

        assert result == ret
