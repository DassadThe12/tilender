# Tilender

## Jak uruchomić

1. zainstalować `docker` oraz `docker-compose`
   1. <https://docs.docker.com/get-docker/>
   2. docker-compose powinien być zainstalowany razem z Docker Desktop. Jeżeli tak nie jest, tutaj znajduję się insrukcjie jak zainstalować: <https://docs.docker.com/compose/install/>
2. w katalogu z plikem docker-compose.yaml należy uruchomoć komendę `docker-compose up --build`
3. należy utuchomoć conajmniej jeden scraper (informacje niżej)

## Jak zatrzymać

należy uruchomoć komendę `docker-compose down`. jeżeli zajdzie potrzeba usunięcia danych z bazy komenda wygląda następująco: `docker-compose down --volumes`

## usługi w dockerze widoczne z zewnątrz

* [:80](localhost:80) Swagger. Uwaga! Wywala error CORS jeżeli pliku nie znalazło
* [:81](localhost:81) Mongo-express. Gui-administracja MongoDB
* [:6800](localhost:82) Scrapyd. Do "zdalnego" uduchamiania scrapera
* [:25500](localhost:25500) Serwer do serwowania jsonów do swagger-ui
* [:27017](localhost:27017) MongoDB. baza danych NoSql

## Scraper

### Konfiguracja zmiennymi środowiskowymi

>przykładowy plik .env znajduje się w `'/.env'`

* `'DEBUG' (bool)`: zmienia środowiska pomiędzy Produkcją i Rozwojowym **wymagane**
* `'db_host' (str)`: host bazy MongoDB, domyślnie `localhost`
* `'db_port' (str)`: port basy MongoDB, domyślnie `27017`
* `'USE_FIREBASE' (bool)`: Czy używać Firebase zamiast Mongo? Domyślnie `~DEBUG`
* `'FIREBASE_COLLECTION' (str)`: Nazwa kolekcji w Firebase, **wymagane jeżeli `USE_FIREBASE==True`**

### Uruchomienie

Docker-compose musi być uruchomiony

1. Bez instalacji lokalnie

    >dokumentacja scrapyd <https://scrapyd.readthedocs.io/en/stable/api.html>

    Domyślnie żaden scraper się nie uruchomi, należy wysłać request do scrapyd

    ```bash
    curl http://localhost:6800/schedule.json -d project=default -d spider=<NAZWA>
    ```

    Gdzie `<NAZWA>` może mieć wartości `Castorama` albo `Cersanit`  
    albo wartości zwracane przez

    ```bash
    curl http://localhost:6800/listspiders.json?project=default
    ```

2. Z instalacją lokalnie

    ```bash
    pip install -r ./scraper/requirements.txt
    scrapy crawl <NAZWA>
    ```

    Gdzie `<NAZWA>` może mieć wartości `Castorama` albo `Cersanit`

3. Komenda wewnątrz kontenera

    ```bash
    scrapy crawl <NAZWA>
    ```

    Gdzie `<NAZWA>` może mieć wartości `Castorama` albo `Cersanit`

### Rekomendacje

Jeżeli Firebase jest w użyciu warto wejść w `/scraper/pipelines` linijki 25 i ustawić zmienną `item_limit` na niską wartość oraz usunąć `#` z linijki 48 aby uruchomić limiter scrapowanych przedmiotów
