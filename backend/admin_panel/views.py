from typing import OrderedDict
from rest_framework.exceptions import ValidationError
from admin_panel.pagination_handler import PaginationHandlerMixin
from rest_framework import status
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework import serializers
from rest_framework.pagination import LimitOffsetPagination

from scraper.models import DjangoTile


class NormalizedTagsSerializer(serializers.Serializer):
    mrozoodpornosc = serializers.BooleanField(default=None)
    wewnatrz = serializers.BooleanField(default=None)
    zewnatrz = serializers.BooleanField(default=None)
    rodzaj_powierzchni = serializers.CharField(default=None)
    liczba_sztuk = serializers.CharField(default=None)
    gwarancja = serializers.CharField(default=None)
    grubosc = serializers.CharField(default=None)
    wymiary = serializers.CharField(default=None)


class RequestSerializer(serializers.Serializer):
    shop = serializers.CharField(default=None)
    shop_url = serializers.CharField(default=None)
    name = serializers.CharField(default=None)
    image_url = serializers.CharField(default=None)
    average_color = serializers.CharField(default=None)
    average_color_name = serializers.CharField(default=None)
    pattern = serializers.CharField(default=None)
    type = serializers.CharField(default=None)
    id = serializers.CharField(default=None)

    normalized_tags = NormalizedTagsSerializer(required=False)

    def validate(self, attrs):
        unknown_keys = set(self.initial_data.keys()) - set(
            (*self.fields.keys(), "normalized_tags")
        )
        if len(unknown_keys) > 0:
            raise ValidationError(f"Unknown fields recived: {unknown_keys}")
        return attrs

    def prepare_filter_kwargs(self):
        ret = dict(self.data.copy())
        if ret.get("id") is not None:
            ret["hash"] = ret.get("id")
            del ret["id"]

        if ret.get("normalized_tags") is not None:
            ret["normalized_tags"] = dict(ret["normalized_tags"])
            if ret["normalized_tags"] == {}:
                del ret["normalized_tags"]
        return ret


class ResponseSerializer(serializers.ModelSerializer):
    normalized_tags = NormalizedTagsSerializer()
    id = serializers.CharField(source="hash")

    class Meta:
        model = DjangoTile
        exclude = ["tags", "hash"]


class All(APIView, PaginationHandlerMixin):
    pagination_class = LimitOffsetPagination

    def get(self, request):
        tiles_q = DjangoTile.objects.all()
        page = self.paginate_queryset(tiles_q)
        if page is None:
            response = ResponseSerializer(tiles_q, many=True)
        else:
            response = self.get_paginated_response(
                ResponseSerializer(page, many=True).data
            )
        return Response(response.data, status=status.HTTP_200_OK)


class Search(APIView, PaginationHandlerMixin):
    pagination_class = LimitOffsetPagination

    def post(self, request):
        data = RequestSerializer(data=request.data, partial=True)
        if not data.is_valid():
            return Response(data.errors, status=status.HTTP_400_BAD_REQUEST)

        tiles_q = DjangoTile.objects.filter(**data.prepare_filter_kwargs()).all()

        page = self.paginate_queryset(tiles_q)
        if page is None:
            response = ResponseSerializer(tiles_q, many=True)
        else:
            response = self.get_paginated_response(
                ResponseSerializer(page, many=True).data
            )

        return Response(response.data, status=status.HTTP_200_OK)
