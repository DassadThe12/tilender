from django.urls import path
from django.urls.conf import include

from . import views

urlpatterns = [
    path("search/", view=views.Search.as_view()),
    path("all/", view=views.All.as_view()),
]
