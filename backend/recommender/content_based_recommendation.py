import pandas as pd
from .jaccard_index import jaccard_similarity
import json

def content_recommend(tile_list, liked, disliked):
    data_set = pd.DataFrame.from_records(tile_list.values())
    #print(data_set)

    #create dataframe with all liked tiles
    liked_tiles = data_set.loc[data_set['hash'].isin(liked)]

    similar_tiles = []
    #print(recommended_tiles)

    for row in data_set.itertuples():
        evaluated_element = jaccard_similarity(row, liked_tiles)

        if(evaluated_element is True):
            similar_tiles.append(row)

    recommended_tiles = pd.DataFrame(similar_tiles)
    #remove redundant if there are any
    rm_redundant_result = recommended_tiles.drop_duplicates(subset=['hash'])
    #remove disliked tiles
    rm_disliked_result = rm_redundant_result.drop(rm_redundant_result[rm_redundant_result['hash'].isin(disliked)].index, inplace=False)
    #remove index field/column
    rm_disliked_result = rm_disliked_result.drop(columns='Index')
    #remove id field/column
    rm_disliked_result = rm_disliked_result.drop(columns='id')

    #convert format
    #metoda 1
    #result = rm_disliked_result.to_json(orient='records')
    #result = json.loads(result)
    
    #metoda 2
    #result = rm_disliked_result.T.to_dict().values()
    
    #metoda 3
    result = rm_disliked_result.to_dict('records')
    
    return result

