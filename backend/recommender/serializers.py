from rest_framework import serializers

from scraper.models import DjangoTile

class DecideeSerializer(serializers.ModelSerializer):
    class Meta:
        model = DjangoTile
        exclude = ("id",)
        #fields = "__all__"


class PreferencesSerializer(serializers.Serializer):
    liked = serializers.ListField(min_length=1, child=serializers.CharField())
    disliked = serializers.ListField(allow_empty=True, child=serializers.CharField())
    


