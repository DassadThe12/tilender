import random
from os import stat
from collections import OrderedDict

from django.core.checks import messages
from django.http import response
from drf_yasg import openapi
from drf_yasg.utils import swagger_auto_schema
from rest_framework import status
from rest_framework.response import Response
from rest_framework.views import APIView

from scraper.models import DjangoTile

from .serializers import DecideeSerializer, PreferencesSerializer
from .content_based_recommendation import content_recommend

# Create your views here.
class Decidee(APIView):
    @swagger_auto_schema(
        responses={
            200: openapi.Response(
                description="Response", schema=DecideeSerializer(many=False)
                ),
            400: "Invalid input",
        },
        tags=["Decidee"],
    )
    def get(self, response):
        ls = list(DjangoTile.objects.all())
        data = random.choice(ls)
        if type(data.tags) == OrderedDict:
            data.tags=dict(data.tags)
        if type(data.normalized_tags) == OrderedDict:
            data.normalized_tags=dict(data.normalized_tags)
        return Response(DecideeSerializer(data, many=False).data, status=status.HTTP_200_OK)

class Preferences(APIView):
    @swagger_auto_schema(
        request_body=PreferencesSerializer(),
        responses={
            201: openapi.Response(description="Response", schema=PreferencesSerializer()),
            400: "Invalid input",
        },
        tags=["Preferences"],
    )
    def post(self, request):
        prefs = PreferencesSerializer(data=request.data)
        if prefs.is_valid():
            all_tiles = DjangoTile.objects.all()
            response_data = content_recommend(all_tiles, prefs.data["liked"], prefs.data["disliked"])

            return Response(response_data, status=status.HTTP_200_OK)
        else:
            return Response(prefs.errors, status=status.HTTP_400_BAD_REQUEST)
