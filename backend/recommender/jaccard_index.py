import pandas as pd
import numpy as np
from sklearn.metrics import jaccard_score

def jaccard_similarity(row_to_eval, liked_tiles):
    is_similar = False
    row_to_evaluate = extract_tags(row_to_eval)
        
    for row in liked_tiles.itertuples():
        tmp_list = extract_tags(row)
        jaccard_index = jaccard_score(tmp_list, row_to_evaluate, average='weighted')
        #jaccard_index = jaccard_score(tmp_list, row_to_evaluate, average='micro')
        #jaccard_index = jaccard_score(tmp_list, row_to_evaluate, average='macro')
        
        if(jaccard_index > 0.6):
            is_similar = True
            return is_similar

    return is_similar


def extract_tags(row):
    tag_list = list(row.normalized_tags.values())
    tag_list.append(row.average_color_name)
    tag_list.append(row.average_color)
    tag_list.append(row.pattern)

    return tag_list
