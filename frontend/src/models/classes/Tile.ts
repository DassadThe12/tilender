import ReviewedTile from "@/models/classes/ReviewedTile";
import PreferenceTypes from "@/models/enums/PreferenceTypes";
import iTile from "@/models/interfaces/iTile";
import NormalisedTags from "@/models/classes/NormalisedTags";

export default class Tile implements iTile {

    average_color: string;
    average_color_name: string;
    id: string;
    image_url: string;
    name: string;
    normalized_tags: NormalisedTags;
    pattern: string;
    shop: string;
    shop_url: string;
    type: string;

    constructor(data: iTile) {
        this.id = data.id;
        this.name = data.name;
        this.average_color = data.average_color;
        this.average_color_name = data.average_color_name;
        this.image_url = data.image_url;
        this.pattern = data.pattern;
        this.shop = data.shop;
        this.shop_url = data.shop_url;
        this.type = data.type;
        this.normalized_tags = data.normalized_tags;
    }

    like(): ReviewedTile {
        return this.definePreference(PreferenceTypes.POSITIVE);
    }

    dislike(): ReviewedTile {
        return this.definePreference(PreferenceTypes.NEGATIVE);
    }

    private definePreference(preference: PreferenceTypes): ReviewedTile {
        return new ReviewedTile(this, preference);
    }

}
