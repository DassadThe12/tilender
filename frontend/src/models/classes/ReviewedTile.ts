import PreferenceTypes from "@/models/enums/PreferenceTypes";
import Tile from "@/models/classes/Tile";

export default class ReviewedTile {

    tile: Tile;
    preference: PreferenceTypes;

    constructor(tile: Tile, preference: PreferenceTypes) {
        this.tile = tile;
        this.preference = preference;
    }

}
