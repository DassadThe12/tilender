export default interface NormalisedTags {
    mrozoodpornosc: boolean;
    wewnatrz: boolean;
    zewnatrz: boolean;
    rodzaj_powierzchni: string;
    liczba_sztuk: number;
    gwarancja: string;
    rozmiar_plytki: string;
    grubosc: string;
}
