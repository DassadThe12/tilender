import iTile from "@/models/interfaces/iTile";
import NormalisedTags from "@/models/classes/NormalisedTags";

// eslint-disable-next-line
export const mockRequest = async function (response: any): Promise<any> {
    return new Promise(
        (resolve) => {
            setTimeout(() => {
                resolve(response);
            }, 1100);
        });
};

const mockTileI: iTile = {
    average_color: "",
    average_color_name: "",
    id: "",
    image_url: "",
    name: "",
    normalized_tags: {
        grubosc: "",
        gwarancja: "",
        liczba_sztuk: 0,
        mrozoodpornosc: false,
        rodzaj_powierzchni: "",
        rozmiar_plytki: "",
        wewnatrz: false,
        zewnatrz: false

    },
    pattern: "",
    shop: "",
    shop_url: "",
    type: ""
}

export const mockTiles: Array<iTile> = [
    {
        average_color: "",
        average_color_name: "",
        id: "mock-tile-id-1",
        image_url: "https://media.castorama.pl/media/catalog/product/cache/0/small_image/266x266/040ec09b1e35df139433887a97daa66f/G/r/Gres_szkliwiony_Odys_Ceramstic_60_x_60_cm_ciemnobezowy_1_44_m2-418230-538570.jpg",
        name: "test-tile-1",
        normalized_tags: {
            grubosc: "",
            gwarancja: "",
            liczba_sztuk: 0,
            mrozoodpornosc: false,
            rodzaj_powierzchni: "",
            rozmiar_plytki: "",
            wewnatrz: false,
            zewnatrz: false

        },
        pattern: "",
        shop: "",
        shop_url: "https://www.castorama.pl/gres-szkliwiony-odys-ceramstic-60-x-60-cm-ciemnobezowy-1-44-m2-id-1126348.html",
        type: ""
    },
    {
        average_color: "",
        average_color_name: "",
        id: "mock-tile-id-2",
        image_url: "https://media.castorama.pl/media/catalog/product/cache/0/small_image/266x266/040ec09b1e35df139433887a97daa66f/G/r/Gres_szkliwiony_polerowany_Lavre_Ceramstic_60_x_60_cm_ciemnoszary_1_44_m2-418045-540489.jpg",
        name: "test-tile-2",
        normalized_tags: {
            grubosc: "",
            gwarancja: "",
            liczba_sztuk: 0,
            mrozoodpornosc: false,
            rodzaj_powierzchni: "",
            rozmiar_plytki: "",
            wewnatrz: false,
            zewnatrz: false

        },
        pattern: "",
        shop: "",
        shop_url: "https://www.castorama.pl/gres-szkliwiony-polerowany-lavre-ceramstic-60-x-60-cm-ciemnoszary-1-44-m2-id-1122992.html",
        type: ""
    },
    {
        average_color: "",
        average_color_name: "",
        id: "mock-tile-id-3",
        image_url: "https://media.castorama.pl/media/catalog/product/cache/0/small_image/266x266/040ec09b1e35df139433887a97daa66f/P/l/Plytka_podlogowa_Ultimate_Marble_Colours_59_5_x_59_5_cm_black_polerowana_1_06_m2-417273-491850.jpg",
        name: "test-tile-3",
        normalized_tags: {
            grubosc: "",
            gwarancja: "",
            liczba_sztuk: 0,
            mrozoodpornosc: false,
            rodzaj_powierzchni: "",
            rozmiar_plytki: "",
            wewnatrz: false,
            zewnatrz: false

        },
        pattern: "",
        shop: "",
        shop_url: "https://www.castorama.pl/plytka-podlogowa-ultimate-marble-colours-59-5-x-59-5-cm-black-polerowana-1-06-m2-id-1101452.html",
        type: ""
    }
];

export const randomTile = (): iTile => {

    const tilesData = mockTiles;
    return tilesData[Math.floor(Math.random() * tilesData.length)];

}

