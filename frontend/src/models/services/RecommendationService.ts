import ReviewedTile from "@/models/classes/ReviewedTile";
import PreferenceTypes from "@/models/enums/PreferenceTypes";
import Tile from "@/models/classes/Tile";
import iTile from "@/models/interfaces/iTile";
import axios from "axios";

import {mockRequest, mockTiles, randomTile} from "./MockService";
import HttpResponse from "@/models/interfaces/HttpResponse";

export class RecommendationService {

    decidee: Tile | undefined;
    preferences: Array<ReviewedTile>;
    recommendations: Array<Tile>;

    constructor() {
        this.decidee = undefined;
        this.preferences = [];
        this.recommendations = [];
    }

    get positives(): Array<ReviewedTile> {
        return this.preferences.filter((item: ReviewedTile) => { return item.preference === PreferenceTypes.POSITIVE });
    }

    get negatives(): Array<ReviewedTile> {
        return this.preferences.filter((item: ReviewedTile) => { return item.preference === PreferenceTypes.NEGATIVE });
    }

    async getNewDecidee(): Promise<Tile> {
        // const response: iTile = randomTile();
        // return mockRequest(response).then((data: iTile) => {
        //     return new Tile(data);
        // });
        let id = "";

        return axios.get("http://localhost:8001/recommend/decidee/")
            .then((response: HttpResponse<iTile>)=>{
                // @ts-ignore
                id = response.data.hash;
                return new Tile({ ...response.data });
            }).then(tile => {
                tile.id = id;
                return tile;
            });
    }

    setNewDecidee = () => {
        this.getNewDecidee().then((newDecidee: Tile)=>{

            const decideesEquality: boolean = Object.keys(newDecidee).every((key: string) => {
                // eslint-disable-next-line
                // @ts-ignore
                return this.decidee[key] === newDecidee[key]
            });

            if (decideesEquality) {
                this.setNewDecidee();
            } else this.decidee = newDecidee;

        })
    }

    async getRecommendations(): Promise<Array<Tile>> {
        // const response: Array<iTile> = mockTiles;
        // return mockRequest(response).then((data: Array<iTile>)=>{
        //     return data.map((item: iTile) => { return new Tile(item) });
        // })
        if(this.preferences.length > 0) {
            const requestBody = {
                liked: this.positives.length > 0 ? this.positives.map((item: ReviewedTile)=>{ return item.tile.id }) : [],
                disliked: this.negatives.length > 0 ? this.negatives.map((item: ReviewedTile)=>{ return item.tile.id }) : []
            }

            return axios.post("http://localhost:8001/recommend/preferences/", requestBody)
                .then((response: HttpResponse<Array<iTile>>) => {
                    return response.data.map((item: iTile) => {
                        return new Tile(item);
                    });
                });
        }
        else return [];
    }

    setRecommendations = () => {
        this.getRecommendations().then(((recommendations: Array<Tile>)=>{
            this.recommendations = recommendations;
        }));
    }

    likeDecidee = () => {
        if(this.decidee) {

            this.preferences.push(this.decidee.like());

            this.setNewDecidee();
            this.setRecommendations();

        }
    }

    dislikeDecidee = () => {
        if(this.decidee) {

            this.preferences.push(this.decidee.dislike());

            this.setNewDecidee();
            this.setRecommendations();

        }
    }

    get hasMinimumData(): boolean {
        return this.positives.length > 2;
    }

}

const RecommendationServiceInstance = new RecommendationService();
export default RecommendationServiceInstance;
