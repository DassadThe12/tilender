import ReviewedTile from "@/models/classes/ReviewedTile";
import PreferenceTypes from "@/models/enums/PreferenceTypes";
import Tile from "@/models/classes/Tile";
import iTile from "@/models/interfaces/iTile";
import axios from "axios";

import {mockRequest, mockTiles, randomTile} from "./MockService";
import TilesPaginationResponse from "@/models/interfaces/TilesPaginationResponse";
import HttpResponse from "@/models/interfaces/HttpResponse";
import FiltersParams from "@/models/interfaces/FiltersParams";

export class TilesCrudService {

    tilesList: Array<Tile>;
    next: string;
    nextFiltered: string;
    allTilesCount: number;
    allFilteredCount: number;
    lastFilters: FiltersParams;

    constructor() {
        this.tilesList = [];
        this.next = "http://localhost:8001/admin/all/";
        this.allTilesCount = 0;
        this.lastFilters = {
            name: undefined,
            average_color_name: undefined,
            shop: undefined,
            normalized_tags: {
                wewnatrz: undefined,
                zewnatrz: undefined,
                mrozoodpornosc: undefined,
                grubosc: undefined
            }
        };
        this.nextFiltered = "http://localhost:8001/admin/search/";
        this.allFilteredCount = 0;
    }

    async getTiles(): Promise<TilesPaginationResponse> {
        return await axios.get(this.next)
            .then((response: HttpResponse<TilesPaginationResponse>)=>{
                return response.data;
            });
    }

    async getSearch(requestedFilters) {
        this.lastFilters = requestedFilters;
        return await axios.post(this.nextFiltered, requestedFilters)
            .then((response: HttpResponse<TilesPaginationResponse>)=>{
                return response.data;
            });
    }

    setProperties(input: TilesPaginationResponse){
        this.tilesList = input.results.map(item => new Tile(item));
        this.next = input.next;
        this.allTilesCount = input.count;
    }

    setFilteredProperties(input: TilesPaginationResponse){
        this.tilesList = input.results.map(item => new Tile(item));
        this.nextFiltered = input.next;
        this.allFilteredCount = input.count;
    }

    setFilters(searchParams: FiltersParams){
        this.nextFiltered = "http://localhost:8001/admin/search/";
        this.lastFilters = searchParams;
    }

}

const TilesCrudServiceInstance = new TilesCrudService();
export default TilesCrudServiceInstance;
