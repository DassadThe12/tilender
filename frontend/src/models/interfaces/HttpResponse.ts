export default interface HttpResponse<ReturnedDataInterface> {
    status: number;
    data: ReturnedDataInterface;
}
