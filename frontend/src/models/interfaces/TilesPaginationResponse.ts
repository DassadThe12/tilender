import iTile from "@/models/interfaces/iTile";

export default interface TilesPaginationResponse {

    count: number;
    next: string;
    previous: string;
    results: Array<iTile>;

}
