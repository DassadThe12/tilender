import NormalisedTags from "@/models/classes/NormalisedTags";

export default interface iTile {

    id: string;
    normalized_tags: NormalisedTags;
    shop: string;
    shop_url: string;
    name: string;
    image_url: string;
    average_color: string;
    average_color_name: string;
    pattern: string;
    type: string;

}
