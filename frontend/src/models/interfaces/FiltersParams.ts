export default interface FiltersParams {
    name?: string,
    average_color_name?: string,
    shop?: string,
    normalized_tags?: {
        wewnatrz?: boolean,
        zewnatrz?: boolean,
        mrozoodpornosc?: boolean,
        grubosc?: string
    }
}
