enum PreferenceTypes {
    "POSITIVE" = "POSITIVE",
    "NEGATIVE" = "NEGATIVE"
}

export default PreferenceTypes;
