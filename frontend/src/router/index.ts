import Vue from 'vue'
import VueRouter, { RouteConfig } from "vue-router"
import Tilender from '../components/pages/Tilender.vue'
import Admin from '../components/pages/Admin.vue'

Vue.use(VueRouter)

const routes: Array<RouteConfig> = [
    {
        path: '/',
        name: 'Tilender',
        component: Tilender
    },
    {
        path: '/admin',
        name: 'Admin',
        component: Admin
    },
    {
        path: "*",
        redirect: "/"
    }
]

const router = new VueRouter({
    routes
})

export default router
