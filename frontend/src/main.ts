/* eslint-disable */

import Vue from 'vue';
import router from './router';

import App from "./components/pages/App.vue";

import "bootstrap/scss/bootstrap.scss";

// PrimeVue setup:
import "primevue/resources/themes/bootstrap4-light-blue/theme.css";       // theme
import "primevue/resources/themes/saga-blue/theme.css";                   // theme
import "primevue/resources/primevue.min.css";                             // core css
import "primeicons/primeicons.css";                                       // icons

import "./assets/styles.scss";

new Vue({
  router,
  render: h => h(App)
}).$mount('#app')
